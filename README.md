# rcon-bot

A srcds RCON bot for Discord on Go

# ***WARNING!!!*** You're advised to __Direct Message__ this bot to RCON! Please don't do this in a public server, as it will __VERY LIKELY__ leak your RCON password!

## Disclaimers

- Read the big lines above
- This bot stores nothing. Don't believe? Check the source code.
- This bot is provided as-is. The bot, or the author/developer, is not responsible for any damages it (the bot) causes to your servers. You have been warned.

## Invitation link

[![bot](https://gitlab.com/diamondburned/rcon-bot/raw/master/RCONbot.png)](https://discordapp.com/api/oauth2/authorize?client_id=482274423102242836&permissions=26624&scope=bot)

## Bot Usage (Discord commands)

- `./help`
- `./rcon -a [ip:port, integer:integer] -p [password, string] [command, string]`
- `./rcon -a 123.45.67.890:27015 -p "super duper cool password" status`

## Self-host Instructions

1. `cp config.example config`
2. `vim config`, add in Discord token (\<Esc\> + `:x` to save and exit, as usual)
3. Either `go run .` or `go build -o bot` then `./bot`

### Requirements:

1. The `config` file (`ini` or `bash`, whatever you want)

## Credits

- RCON from https://github.com/itzg/rcon-cli
