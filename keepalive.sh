#!/bin/bash

function ontrap() {
	echo "Trapped."
	exit 0
}

trap ontrap SIGINT

pkill -f './bot'

mkdir -p tmp/
[ -f tmp/botlog ] && touch -f tmp/botlog

while true; do
	./bot | tee -a tmp/botlog &> /dev/null
	echo "Bot exited." | tee -a tmp/botlog
	sleep 10
done
