module gitlab.com/diamondburned/rcon-bot

go 1.13

require (
	github.com/SeerUK/minecraft-rcon v0.0.0-20190221212056-6ab996d90449
	github.com/bwmarrin/discordgo v0.20.1
	github.com/james4k/rcon v0.0.0-20120923215419-8fbb8268b60a
	github.com/pkg/errors v0.8.1
	github.com/seeruk/minecraft-rcon v0.0.0-20190221212056-6ab996d90449 // indirect
	gopkg.in/ini.v1 v1.51.0
)
