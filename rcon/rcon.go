package rcon

import (
	"fmt"
	"io"
	"log"
	"strconv"

	mcrc "github.com/SeerUK/minecraft-rcon/rcon"
	"github.com/james4k/rcon"
)

const (
	// Source RCON
	Source = 1

	// Minecraft RCON
	Minecraft = 2
)

//rcon --host HOST --port PORT --password PASSWORD COMMAND
func Run(host, port, password, command string, game int) (string, error) {
	switch game {
	case Source:
		remoteConsole, err := rcon.Dial(host+":"+port, password)
		if err != nil {
			log.Fatal("Failed to connect to RCON server", err)
		}

		defer remoteConsole.Close()

		reqID, err := remoteConsole.Write(command)
		if err != nil {
			return "", fmt.Errorf("Failed to write command.")
		}

		resp, respReqID, err := remoteConsole.Read()
		if err != nil {
			if err == io.EOF {
				return "", nil
			}

			return "", fmt.Errorf("Failed to read command.")
		}

		if reqID != respReqID {
			return "", fmt.Errorf("Weird. This response is for another request.")
		}

		return resp, nil
	case Minecraft:
		portInt, err := strconv.Atoi(port)
		if err != nil {
			return "", fmt.Errorf("Invalid port")
		}

		client, err := mcrc.NewClient(host, portInt, password)
		if err != nil {
			log.Println(err.Error())
			return "", err
		}

		response, err := client.SendCommand(command)
		if err != nil {
			log.Println(err.Error())
			return "", err
		}

		return response, nil
	}

	return "", fmt.Errorf("We'll all die!")
}
