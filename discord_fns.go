package main

import (
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func messageUpdate(session *discordgo.Session, message *discordgo.MessageUpdate) {
	if !strings.HasPrefix(message.Content, prefix) {
		return
	}

	handleMessage(session, message.Message)
}

func messageCreate(session *discordgo.Session, message *discordgo.MessageCreate) {
	if !strings.HasPrefix(message.Content, prefix) {
		return
	}

	if message.Content == prefix+"help" {
		embed := new(discordgo.MessageEmbed)
		embed.Title = "Help!"

		fields := []*discordgo.MessageEmbedField{
			&discordgo.MessageEmbedField{
				Name:  "Usage (Source gameservers):",
				Value: "> `./rcon -a 123.45.67.890:27015 -p \"correct horse battery staple\" status`",
			},
			&discordgo.MessageEmbedField{
				Name:  "Usage (Minecraft gameservers):",
				Value: "> `./mcrc -a 123.45.67.890:27015 -p \"correct horse battery staple\" say Test`",
			},
			&discordgo.MessageEmbedField{
				Name:  "Extended options:",
				Value: "> `--address \"123.45.67.890:27015\" --password \"correct horse battery staple\"`",
			},
		}

		embed.Fields = fields

		_, err := session.ChannelMessageSendEmbed(message.ChannelID, embed)
		if err != nil {
			log.Println("Can't send! ", err)
			return
		}
	}

	handleMessage(session, message.Message)
}

// ComesFromDM Admittedly some of these are just copypasted. Quite sad.
func ComesFromDM(s *discordgo.Session, m *discordgo.Message) (bool, error) {
	channel, err := s.State.Channel(m.ChannelID)
	if err != nil {
		if channel, err = s.Channel(m.ChannelID); err != nil {
			return false, err
		}
	}

	return channel.Type == discordgo.ChannelTypeDM, nil
}

func errToDiscord(s *discordgo.Session, channelID string, err error, deleteIDs ...string) {
	_, err = s.ChannelMessageSend(channelID, "Error: "+err.Error())
	if err != nil {
		log.Println("Error sending message with error", err, "to channel", channelID)
	}

	s.ChannelMessagesBulkDelete(channelID, deleteIDs)
}
