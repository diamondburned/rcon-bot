/*

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.

*/

package main

import (
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	//"reflect"
	"encoding/csv"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/diamondburned/rcon-bot/rcon"
	"gopkg.in/ini.v1"
)

var (
	ErrInvalidArgs        = errors.New("Error! Missing argument! Please refer to `./help`.")
	ErrInvalidCredentials = errors.New("Error! Invalid credentials! Please double check and maybe quote variables.")

	ErrNotDM = errors.New("The bot couldn't securely remove the message and therefore didn't process the RCON. Please enable the \"Manage Messages\" permission for the bot before using RCON.")
)

func quickini(ini *ini.File, key string) string {
	return ini.Section("").Key(key).String()
}

func main() {
	cfg, err := ini.Load("config")
	if err != nil {
		log.Fatalln("Failed to parse ./config:", err)
	}

	discord, err := discordgo.New("Bot " + quickini(cfg, "TOKEN"))
	if err != nil {
		log.Fatalln("Failed to create a new Discord session:", err)
	}

	discord.AddHandler(messageCreate)
	discord.AddHandler(messageUpdate)

	if err := discord.Open(); err != nil {
		log.Fatalln("Failed to connect to Discord:", err)
	}

	defer discord.Close()

	log.Println("Bot is ready.")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

// prefix is the command prefix
const prefix = "./"

func whatRCON(msg string) int {
	commands := []string{"rcon", "mcrc"}
	for k, cmd := range commands {
		if strings.HasPrefix(msg, prefix+cmd) {
			return k + 1
		}
	}

	return 0
}

func handleMessage(session *discordgo.Session, message *discordgo.Message) {
	if sig := whatRCON(message.Content); sig > 0 {
		waitmsg, err := session.ChannelMessageSend(message.ChannelID, "Sending...")
		if err != nil {
			errToDiscord(session, message.ChannelID,
				errors.Wrap(err, "Failed to send msg"))
			return
		}

		fromDM, err := ComesFromDM(session, message)
		if err != nil {
			errToDiscord(session, message.ChannelID,
				errors.Wrap(err, "Failed to send msg"), waitmsg.ID)
			return
		}

		if fromDM == false {
			err = session.ChannelMessageDelete(message.ChannelID, message.ID)
			if err != nil {
				errToDiscord(session, message.ChannelID, ErrNotDM, waitmsg.ID)
				return
			}
		}

		switch sig {
		case rcon.Source, rcon.Minecraft:
			err = RCONcmd(session, message, waitmsg, prefix, sig)
		}

		if err != nil {
			errToDiscord(session, message.ChannelID, err, waitmsg.ID)
		}
	}
}

// RCONcmd Just a shortcut for repeated command execution
func RCONcmd(session *discordgo.Session, message, waitmsg *discordgo.Message, prefix string, game int) error {
	input := csv.NewReader(strings.NewReader(message.Content))
	input.Comma = ' ' // delimiter

	args, err := input.Read()
	if err != nil {
		return errors.Wrap(err, "broken input")
	}

	args = args[1:] // shift

	var address, password string
	var entry, entry1 int = 0, 0
	command_arr := []string{}

	for range args {
		entry1 = entry + 1

		switch args[entry] {
		case "-a", "--address":
			address = args[entry1]

		case "-p", "--password":
			password = args[entry1]

		default:
			if (address != "") && (password != "") {
				if args[entry] != address && args[entry] != password { // #bashftw
					command_arr = append(command_arr, args[entry])
				}
			}
		}

		entry = entry1
	}

	command := strings.Join(command_arr, " ")

	if (address == "") || (password == "") || (command == "") {
		return ErrInvalidArgs
	}

	address_args := strings.Split(address, ":")

	host, port := address_args[0], address_args[1]

	stdout, err := rcon.Run(host, port, password, command, game)
	if err != nil {
		return ErrInvalidCredentials
	}

	embed := new(discordgo.MessageEmbed)
	embed.Title = "Output for `" + address + "`"
	if len(stdout) > 1806 {
		embed.Description = "Message too long!"
	} else {
		embed.Description = "```" + stdout + "```"
	}

	var success = "success"

	_, err = session.ChannelMessageEditComplex(&discordgo.MessageEdit{
		Content: &success, // dgo why?
		Embed:   embed,

		ID:      waitmsg.ID,
		Channel: message.ChannelID,
	})

	if err != nil {
		return errors.Wrap(err, "Can't update old message with results")
	}

	return nil
}
